import json
import io


class Packet:
    fixedHeaderSize = 4

    def __init__(self):
        # packet data
        self.header = None
        self.content = None
        # what is sent on the "wires"
        self.buffer = b""

        # Setup execution stages
        self.expectedByteCount = self.fixedHeaderSize
        self.stage = self.readProtoHeader
        self.nextStage = {
            self.readProtoHeader: self.readHeader,
            self.readHeader: self.readContent,
            self.readContent: self.readyStage
        }

    def reset(self):
        self.header = None
        self.content = None
        self.buffer = b""
        self.expectedByteCount = self.fixedHeaderSize
        self.stage = self.readProtoHeader

    def readyStage(self):
        return self.stage == self.readyStage

    def updateStage(self, stageByteCount):
        """ Update execution stage to next level. """
        self.stage = self.nextStage[self.stage]
        self.expectedByteCount = stageByteCount

    def appendData(self, data):
        """ Add more data into the packet. """
        self.buffer += data

    def canRead(self):
        if self.expectedByteCount is None:
            print("No need to read more bytes.")
            return False
        if len(self.buffer) < self.expectedByteCount:
            print("Need at least", self.expectedByteCount, "bytes.")
            return False
        return True

    def readData(self):
        """ Extract bytes from the buffer. """
        # Debug. This fn should be called only after checking canRead()
        if not self.canRead():
            raise Exception("Trying to read more data than there is.")

        data = self.buffer[:self.expectedByteCount]
        self.buffer = self.buffer[self.expectedByteCount:]

        return data

    def readProtoHeader(self):
        """ Read proto header, that tells the size of the actual header. """
        if not self.canRead():
            return False
        self.updateStage(int(self.readData()))
        return True

    def verifyHeader(self):
        for reqEntry in (
            # "byteorder",
            "content-length",
            "content-type",
            # "content-encoding",
        ):
            if reqEntry not in self.header:
                raise ValueError(f"Missing required header entry `{reqEntry}`.")

    def readHeader(self):
        if not self.canRead():
            return False

        self.header = self.json_decode(self.readData(), "utf-8")
        self.verifyHeader()
        # next stage
        self.updateStage(self.header["content-length"])
        return True

    def readContent(self):
        """ Read next expected bytes and interpret them using header info. """
        if not self.canRead():
            return False

        data = self.readData()
        self.content = self.decodeData(data, self.header["content-type"])
        self.updateStage(None) # go to readyStage
        return True

    def read(self, data):
        self.appendData(data)

        while not self.readyStage():
            if not self.stage():
                print("Warn: Stage failed")
                return False
        return True

    def pack(self, content, contentType="text/text"):
        self.reset()
        self.content = content

        # content
        encodedContent = self.encodeData(self.content, contentType)

        # header
        self.header = {
            "content-length": len(encodedContent),
            "content-type": contentType,
        }
        encodedHeader = self.json_encode(self.header, "utf-8")

        # fixed header
        encodedHeaderSize = self.intToByteString(len(encodedHeader), self.fixedHeaderSize)

        # create message
        self.buffer += encodedHeaderSize + encodedHeader + encodedContent
        # update stage
        self.expectedByteCount = None
        self.stage = self.readyStage

    # Helpers
    @staticmethod
    def intToList(value, listSize=4):
        data = [0] * listSize
        index = listSize - 1
        while value > 0 and index >= 0:
            data[index] = value % 10
            value //= 10
            index -= 1
        if value:
            raise ValueError(f"Number {value} is too big.")
        return data

    @staticmethod
    def listToString(list_):
        result = ""
        for elem in list_:
            result += str(elem)
        return result

    @staticmethod
    def intToByteString(value, strSize):
        data = Packet.intToList(value, strSize)
        data = Packet.listToString(data)
        data = data.encode("utf-8")
        return data

    @staticmethod
    def json_encode(obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    @staticmethod
    def json_decode(json_bytes, encoding):
        tiow = io.TextIOWrapper(
            io.BytesIO(json_bytes), encoding=encoding, newline=""
        )
        obj = json.load(tiow)
        tiow.close()
        return obj

    @staticmethod
    def encodeData(data, contentType):
        if "text/text" == contentType:
            return data.encode("utf-8")
        if "text/json" == contentType:
            return Packet.json_encode(data, "utf-8")
        raise ValueError(f"Cannot convert to {str(contentType)}")

    @staticmethod
    def decodeData(data, contentType):
        if "text/text" == contentType:
            return data.decode("utf-8")
        if "text/json" == contentType:
            return Packet.json_decode(data, "utf-8")
        raise ValueError(f"Cannot convert from {str(contentType)}")