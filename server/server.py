import sys
import socket
import selectors
import types
from data import *
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql import Row
from Packet import Packet

# Get best select() function
sel = selectors.DefaultSelector()
# Used for packing response
packetCreator = Packet()


def getType(columnName):
    colType = {
        "Nr": IntegerType(),
        "Year": IntegerType(),
        "Month": IntegerType(),
        "DayofMonth": IntegerType(),
        "DayOfWeek": IntegerType(),
        "CRSDepTime": IntegerType(),
        "FlightNum": IntegerType(),
        "Distance": IntegerType(),
        "DepTime": DoubleType(),
        "ArrTime": DoubleType(),
        "CRSArrTime": DoubleType(),
        "ActualElapsedTime": DoubleType(),
        "CRSElapsedTime": DoubleType(),
        "AirTime": DoubleType(),
        "ArrDelay": DoubleType(),
        "DepDelay": DoubleType(),
        "TaxiIn": DoubleType(),
        "TaxiOut": DoubleType(),
        "CarrierDelay": DoubleType(),
        "WeatherDelay": DoubleType(),
        "NASDelay": DoubleType(),
        "SecurityDelay": DoubleType(),
        "LateAircraftDelay": DoubleType(),
        "UniqueCarrier": StringType(),
        "TailNum": StringType(),
        "Origin": StringType(),
        "Dest": StringType(),
        "CancellationCode": StringType(),
        "Cancelled": IntegerType(),
        "Diverted": IntegerType()
    }
    return colType[columnName]


def getSchema():
    # Prep the schema: a StructType() containing a StructField[] list
    # StructField is a tuple of 3:
    #   ("column_name", column_type, nullable=True/False)
    #
    #   * column_type is an object from `pyspark.sql.types` (ex: IntegerType(), FloatType())
    #
    colNames = "Nr,Year,Month,DayofMonth,DayOfWeek,DepTime,CRSDepTime,ArrTime,CRSArrTime,UniqueCarrier,FlightNum," \
               "TailNum,ActualElapsedTime,CRSElapsedTime,AirTime,ArrDelay,DepDelay,Origin,Dest,Distance,TaxiIn," \
               "TaxiOut,Cancelled,CancellationCode,Diverted,CarrierDelay,WeatherDelay,NASDelay,SecurityDelay," \
               "LateAircraftDelay"
    fields = [StructField(field_name, getType(field_name), True) for field_name in colNames.split(sep=',')]
    return StructType(fields)


def getTaxiInOut(df, spark):
    def getHour(hourAsDouble):
        return int(hourAsDouble) // 100
    # SELECT ... FROM df --- extracting the hour also
    df = spark.createDataFrame(
        df.rdd.map(lambda row: Row(
            CRSDepTime=getHour(row["CRSDepTime"]),
            CRSArrTime=getHour(row["CRSArrTime"]),
            TaxiIn=row["TaxiIn"],
            TaxiOut=row["TaxiOut"],
        ))
    )
    # save df in memory for better speed
    df.persist()
    # SELECT ... AS "hour", ... AS "countdown" FROM df
    taxiInHours = df.select("CRSArrTime", "TaxiIn").toDF("hour", "countdown")
    taxiOutHours = df.select("CRSDepTime", "TaxiOut").toDF("hour", "countdown")
    return taxiInHours, taxiOutHours


def task1(df, spark):
    print("\n1. Find out the top 5 most visited destinations.")
    # SELECT "Dest", count("Nr") AS "Visit Count" from df
    # GROUP BY "Dest"
    # ORDER BY "Visit Count" DESC
    #
    return df.groupBy("Dest")\
        .agg({"Nr": "count"})\
        .sort("count(Nr)", ascending=False)\
        .limit(5)


def task2(df, spark):
    print("\n2. Which month has seen the most number of cancellations due to bad weather?")
    # SELECT "Month", count("Nr") as "Cancellation Count" FROM df
    # WHERE "Cancelled" = 1 AND "CancellationCode" = 'B'
    # GROUP BY "Month"
    # ORDER BY "Cancellation Count"
    #
    return df.filter(df['Cancelled'] == 1).filter(df['CancellationCode'] == 'B')\
        .groupBy("Month")\
        .agg({"Nr": "count"})\
        .sort("count(Nr)", ascending=False)\
        .limit(1)


def task3(df, spark):
    print("\n3. Top ten origins with the highest AVG departure delay")
    # SELECT "Origin", avg("DepDelay") "AVG Departure Delay" FROM df
    # GROUP BY "Origin"
    # ORDER BY "AVG Departure Delay"
    #
    return df.groupBy("Origin")\
        .agg({"DepDelay": "avg"})\
        .sort("avg(DepDelay)", ascending=False)\
        .limit(10)


def task4(df, spark):
    print("\n4. Which route (origin & destination) has seen the maximum diversion?")
    # SELECT "Origin", "Dest", count("Nr") AS "Diversion Count" FROM df
    # WHERE "Diverted" = 1
    # GROUP BY "Origin", "Dest"
    # ORDER BY "Diversion Count" DESC
    #
    return df.filter(df["Diverted"] == 1) \
        .groupBy(df["Origin"], df["Dest"]) \
        .agg({"Nr": "count"}) \
        .sort("count(Nr)", ascending=False)\
        .limit(1)


def task5(df, spark):
    def getMaxTaxiTimePerHour(p_taxiHours):
        # SELECT "hour", max("countdown") AS "MAX Taxi Time" FROM p_taxiHours
        # WHERE "countdown" != null
        # GROUP BY "hour"
        #
        return p_taxiHours.filter(p_taxiHours["countdown"].isNotNull()) \
            .groupBy("hour") \
            .agg({"countdown": "max"})

    print("\n5. Which hours had the highest taxi In & taxi out time?")
    # Get taxi in per "hour" and taxi out per "hour"
    taxiInHours, taxiOutHours = getTaxiInOut(df, spark)
    taxiInHours = getMaxTaxiTimePerHour(taxiInHours)
    taxiOutHours = getMaxTaxiTimePerHour(taxiOutHours)

    # Get total taxi time per "hour"
    taxiHours = taxiInHours.union(taxiOutHours)
    taxiHours = taxiHours.groupBy("hour")\
        .agg({"max(countdown)": "sum"})\
        .toDF("hour", "taxiIn + taxiOut")\
        .sort("taxiIn + taxiOut", ascending=False)
    return taxiHours.limit(10)


def dfToString(df):
    string = "{"
    for row in df.toJSON().toLocalIterator():
        string += row + ",\n"
    return string[:-2] + "}"


def setupSparkSession():
    spark = SparkSession \
        .builder \
        .appName("PythonPi") \
        .getOrCreate()

    # load data using custom scheme
    df = spark.read.format("csv") \
        .option("header", "true") \
        .schema(getSchema()) \
        .load("DelayedFlights.csv")
    # save in memory for speed
    df.persist()
    return spark, df


def setupSock(host_, port_):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host_, port_))
    return sock


def acceptConnection(sock):
    """Accept a connection on @sock and register it for READ & WRITE events."""
    conn, addr = sock.accept()
    print("Accepted connection from", addr)

    # setup session data
    data = types.SimpleNamespace(
        addr=addr,
        tosend=b"",
        packet=Packet()
    )

    # listen for READ events on conn socket
    conn.setblocking(False)
    sel.register(conn, selectors.EVENT_READ, data=data)


def processRequest(sock, data):
    """ Process client request. Called when READ even is triggered on @sock."""
    recvData = sock.recv(1024)
    if recvData:
        # print("received", repr(recvData), "from address", data.addr, "+")
        data.packet.read(recvData)
        # check if packet was fully read
        if data.packet.readyStage():
            # print("Received packet successfully.")
            # print("Header:")
            # print(data.packet.header)
            # print("Content:")
            # print(data.packet.content)
            # done reading a packet
            if data.packet.buffer:
                print("Request was too long/more packets where send.")
                print("Discarding extra.")
            # listen for write event now
            sel.modify(sock, selectors.EVENT_WRITE, data=data)
    else:
        print("Closing connection to", data.addr)
        sel.unregister(sock)
        sock.close()


def tasks():
    func = [
        task1,
        task2,
        task3,
        task4,
        task5,
    ]
    return func

def tasksDescription():
    desc = [
        "1. Find out the top 5 most visited destinations.",
        "2. Which month has seen the most number of cancellations due to bad weather?",
        "3. Top ten origins with the highest AVG departure delay?",
        "4. Which route (origin & destination) has seen the maximum diversion?",
        "5. Which hours had the highest taxi In & taxi out time?"
    ]
    return desc


def response(sock, data, df, spark):
    """ Send response after processingRequest. Called when WRITE event is triggered on @sock (which is always on any
    sock). """
    if not data.tosend:
        if not data.packet.readyStage():
            print("Err")
        taskNr = int(data.packet.content) - 1
        if taskNr >= len(tasks()):
            packetCreator.pack("Invalid task number " + str(taskNr + 1), "text/text")
        else:
            task = tasks()[taskNr]
            res = task(df, spark)
            res.show()  # fis
            resData = tasksDescription()[taskNr] + "\n" + dfToString(res)
            packetCreator.pack(resData, "text/text")
        data.tosend = packetCreator.buffer
        # inb = packet.write(res.show())
    if data.tosend:
        # print("sending", repr(data.tosend), "to", data.addr)
        sentBytesCount = sock.send(data.tosend)
        data.tosend = data.tosend[sentBytesCount:]  # drop sent bytes
        if not data.tosend:
            # print("send successful")
            # Now listen for a request
            data.packet = Packet()
            sel.modify(sock, selectors.EVENT_READ, data=data)


def service_connection(key, eventsMask, df, spark):
    """ Service the connection: set it up or processRequest and respond."""
    sock = key.fileobj
    data = key.data
    if eventsMask & selectors.EVENT_READ:
        processRequest(sock, data)
    if eventsMask & selectors.EVENT_WRITE:
        response(sock, data, df, spark)


def eventLoop(df, spark):
    try:
        while True:
            changedList = sel.select(timeout=None)  # None -> block until an event is triggered
            for key, eventsMask in changedList:
                if key.data is None:
                    sock = key.fileobj
                    acceptConnection(sock)
                else:
                    service_connection(key, eventsMask, df, spark)
    except KeyboardInterrupt:
        print("caught keyboard interrupt, exiting")
    finally:
        sel.close()


def main():
    spark, df = setupSparkSession()
    print("SparkSession ready")

    serverSock = setupSock(host, port)
    print("Listening on", f'{host}:{port}')
    serverSock.listen()

    # listen for READ events on socket
    serverSock.setblocking(False)   # non-blocking socket
    sel.register(serverSock, selectors.EVENT_READ, data=None)

    eventLoop(df, spark)


if __name__ == "__main__":
    main()
