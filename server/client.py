import sys
import socket
import selectors
import types
from data import *
from Packet import Packet

sel = selectors.DefaultSelector()

packetCreator = Packet()


def createSessionData(connid, msgs):
    encodedMsgs = []
    for m in msgs:
        packetCreator.pack(m)
        encodedMsgs.append(packetCreator.buffer)
    return types.SimpleNamespace(
        connid=connid,
        packet=None,
        totalSize=sum(len(m) for m in encodedMsgs),
        recvSize=0,
        messages=list(encodedMsgs),
        outb=b"",
    )


def startConnections(host_, port_, numConns, msgs):
    svrAddr = (host_, port_)
    for i in range(0, numConns):
        connid = i + 1
        # setup socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(False)  # non-blocking socket

        print("starting connection", connid, "to", svrAddr)
        sock.connect_ex(svrAddr)
        # setup params
        eventsMask = selectors.EVENT_WRITE
        data = createSessionData(connid, msgs)
        # register sockets for events
        sel.register(sock, eventsMask, data=data)


def serviceConnection(key, eventsMask):
    sock = key.fileobj
    data = key.data
    # processResponse
    if eventsMask & selectors.EVENT_READ:
        recvData = sock.recv(1024)     # Should be ready to read
        if recvData:
            # print("received", repr(recvData), "from connection", data.connid, "+")
            data.packet.read(recvData)
            # check if packet was fully read
            if data.packet.readyStage():
                # print("Received packet successfully.")
                # print("Header:")
                # print(data.packet.header)
                # print("Content:")
                # print(data.packet.content)
                print("Result:")
                print(data.packet.content)
                # Done reading a packet
                if data.packet.buffer:
                    print("Response was too long/more packets where send.")
                    print("Discarding extra.")
                # listen for write event now
                sel.modify(sock, selectors.EVENT_WRITE, data=data)
        # if not recvData:
        else:
            print("closing connection", data.connid)
            sel.unregister(sock)
            sock.close()
    # sendRequest
    if eventsMask & selectors.EVENT_WRITE:
        if not data.outb and data.messages:
            data.outb = data.messages.pop(0)
        if data.outb:
            # print("sending", repr(data.outb), "to connection", data.connid)
            sent = sock.send(data.outb)     # Should be ready to write
            data.outb = data.outb[sent:]
            if not data.outb:
                # print("Send successful.")
                # Now listen for the server response
                data.packet = Packet()
                sel.modify(sock, selectors.EVENT_READ, data=data)
        else:
            # No more to send
            print("closing connection", data.connid)
            sel.unregister(sock)
            sock.close()


def eventLoop():
    try:
        while True:
            changed = sel.select(timeout=1)
            if changed:
                for key, eventsMask in changed:
                    serviceConnection(key, eventsMask)
            # Check for a socket being monitored to continue.
            if not sel.get_map():
                break
    except KeyboardInterrupt:
        print("caught keyboard interrupt, exiting")
    finally:
        sel.close()


def main():
    numConns = 1
    messages = []

    if len(sys.argv) != 2:
        print("Usage: [app] taskNr")
        return

    if not str(sys.argv[1]).isnumeric():
        print("taskNr must be a number")

    messages.append(str(sys.argv[1]))
    startConnections(host, int(port), int(numConns), messages)

    eventLoop()


if __name__ == "__main__":
    main()
