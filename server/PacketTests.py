from Packet import Packet
import string


if __name__ == "__main__":
    packet = Packet()
    dictionary = {
        "task1": 1,
        "task2": 2,
        "task3": 3,
        "task4": 4,
        "task5": 5
    }
    packet.pack(dictionary, contentType="text/json")
    print(packet.buffer)

    decPaket = Packet()
    decPaket.read(packet.buffer[:5])
    decPaket.read(packet.buffer[5:27])
    decPaket.read(packet.buffer[27:52])
    decPaket.read(packet.buffer[52:57])
    decPaket.read(packet.buffer[57:])
    print(decPaket.content['task'])
    print(decPaket.header)

    decPaket.buffer += b'123'
    if decPaket.buffer:
        print("ok")

    print(decPaket.buffer)
